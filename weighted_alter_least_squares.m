function [U, V, outputs] = weighted_alter_least_squares(Y, P, k, linkf, U0, V0, reg)
%% Alternating least squres with weights
%
% Y         : n1-by-n2-by-d observation matrix 
% P         : n1-by-n2-by-d sampling matrix
% k         : rank 
% U0        : init U
% V0        : init V
% reg       : some parameters used in alternating minimization, with fields
    % mu        : l2 regularization for U
    % gamma     : l2 regularization for V
    % kappa     : dynamic ratio = sigma_2^2 / sigma_1^2
    % iteMaxOut : maximum number of iterations for outer loop
    % tolOut    : optimality tolerance in objective

% U         : estimated U
% V         : estimated V
% outputs   : intermediate outputs structure
    
[n1, n2, d] = size(Y);

% init U and V
if isempty(U0) U = randn(n1, k); else U = U0; end
if isempty(V0) V = randn(n2, k); else V = V0; end

% calculate weights w according to kappa
if ~reg.ewflag  
    a = zeros(d,1);
    for t = 1:d
        a(d-t+1) = 1+ (t-1)*reg.kappa; 
    end
    b = sum(1./a);
    for t = 1:d
        w(t) = 1/b * 1/(a(t));
    end
    else
    w = 1/d *ones(d,1);
end

% starting outer loop
iteCntOut = 0;

% initial objective function
obj = obj_function(U, V, Y, P, linkf, reg, w);
outputs.obj(iteCntOut + 1) = obj;


while iteCntOut < reg.iteMaxOut
    
    % update U with V fixed using least squares
    for j = 1:n1
        Au = [];
        yu = [];
        Wu = [];
        for i = 1:d
            P1u = P(j,:,i);
            Au = [Au; V(P1u,:)];
            Wu = [Wu; repmat(w(i)^0.5,sum(P1u),1)];
            yu = [yu; reshape(Y(j,P1u,i), [],1)];
        end
%         xu = inv(Au'* diag(Wu) * Au+ reg.mu* eye(k,k)) * Au' * diag(Wu) * yu;
        Wu2 = repmat(Wu,1,k);
        Au2 = Au.* Wu2;
        yu2 = yu.*Wu;
        xu = inv( Au2' * Au2 + reg.mu* eye(k,k)) * Au2' * yu2;
        U(j,:) = xu';
    end
    % update V with U fixed using least squares
    for j = 1:n2
        Av = [];
        yv = [];
        Wv = [];
        for i = 1:d
            P1v = P(:,j,i);
            Av = [Av; U(P1v,:)];
            Wv = [Wv; repmat(w(i)^0.5,sum(P1v),1)];
            yv = [yv; reshape(Y(P1v,j,i), [],1)];
        end
        Wv2 = repmat(Wv,1,k);
        Av2 = Av.*Wv2;
        yv2 = yv.*Wv;
        xv = inv(Av2'* Av2 + reg.gamma * eye(k,k)) * Av2' *yv2;
%         xv = inv(Av'* diag(Wv) * Av+ reg.gamma* eye(k,k)) * Av' * diag(Wv) * yv;
        V(j,:) = xv';
    end
    
    last_obj = obj;
    obj = obj_function(U, V, Y, P, linkf, reg, w);
    rel_obj = abs(obj - last_obj) / abs(last_obj);
    if rel_obj < reg.tolOut
        % exit when objective don't change much 
        break;
    end
    iteCntOut = iteCntOut + 1;
    outputs.obj(iteCntOut + 1) = obj;    
end

end

% objective function
function obj = obj_function(U, V, Y, P, linkf, reg, w)
    d = size(V,3);
    nllh = 0;
    for i = 1:d
        tmp =  U* V(:,:)';

        UV = tmp(P(:,:,i));
        YP = Y(:,:,i);
        YP = YP(P(:,:,i));
        f = linkf(UV);
        % negative log likelihood 
        if isequal(linkf,(@gauss_link))
            nllh =   nllh  + 0.5 * w(i) * sum((YP - f).^2);
        elseif isequal( linkf, (@logit_link))
            nllh = nllh   -  w(i) * sum( ( YP .* log(f) + (1-YP).*log(1-f)));
        end
    end
    % add regularization
    obj = nllh + 0.5 * reg.mu * norm(U, 'fro')^2 +  0.5 *  reg.gamma * norm(V, 'fro')^2 ;
end