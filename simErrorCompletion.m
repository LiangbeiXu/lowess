% simulation of recovering error for matrix completion (revised)
%% Init
close all;clc; clearvars;
warning('on','all')
% profile on

%% Set parameters
n1 = 100;           % number of rows in matrix
n2 = 50;            % number of columns in matrix
r = 5;              % rank 
d = 4;              % number of time stamps
sigma1 = 0.05;      % std variance of observation noise (Gaussian)
sigma2 = 0.1;       % std variance of perturbation noise (Gaussian)
p = 0.8;            % sampling ratio
repNum = 10;        % number of simulation runs
linkf = @ gauss_link ; % link function, Gaussian for Gaussian observation model

reg.gamma = 0;      % l2 regularization for U
reg.mu = 0;         % l2 regularization for V
reg.iteMaxOut = 10; % maximum number of iterations for outer loop  
reg.tolOut = 1e-4;  % optimality tolerance in objective
reg.ewflag = 0;     % flag: 1 equal weights; 0 computed using dynamic ratio

% parameter space to sim in 
sigma2List = logspace(-2,0.5,15);

% kappaList represents: baseline 1, LOWESS, baseline 2
kappaList  = [1e7 1 1e-7];

%% run simulations
for sidx = 1:length(sigma2List)
    sigma2 = sigma2List(sidx);
    % Generate synthetic data 
    [U, Varray, Marray, Parray] = genSynNIPS(n1, n2, r, d, sigma1, sigma2, p, 1);

    % the true low-rank matrix
    UV= U * reshape(Varray(:,:,d),n2,r)';
    
    % Caculate kappa
    kappaList(2) = sigma2^2/sigma1^2;
    
    for kidx = 1:length(kappaList)
        reg.kappa = kappaList(kidx);
        for repCnt = 1:repNum 
            % solve
            [Uhat, Vhat, outputs] = weighted_alter_least_squares(Marray, ...
                logical(Parray), r, linkf, [], [], reg);
            % estimated low-rank matrix
            UVhat = Uhat*Vhat';
            relE(repCnt) = norm(UVhat - UV, 'fro')^2 / norm(UV,'fro')^2;
        end
        % store  relative error
        rel_error(kidx , sidx) = mean(relE);
    end
end
%% Show and plot results
figure
mark = {'*-', 'o-', '+-', 'v-','^-'};
lcolor = {'r', 'g', 'b', 'c'};

% Baseline 1
semilogx(sigma2List, rel_error(1,:), [mark{1} lcolor{1}]);  hold on

% Baseline 2
semilogx(sigma2List(1:9), rel_error(end,1:9), [mark{3} lcolor{3}]);  hold on

% LOWEMS
semilogx(sigma2List, rel_error(2,:), [mark{2} lcolor{2}]);  hold on

ltext{1} = 'Baseline one';
ltext{2} = 'Baseline two';
ltext{3} = 'LOWEMS';
l1 = xlabel('\sigma_2');
l2 = ylabel('Recovery Error');
l3 = legend(ltext);

set(gca,'FontSize',14);
set(l1,'FontSize',14);
set(l2,'FontSize',14);
set(l3,'FontSize',14);