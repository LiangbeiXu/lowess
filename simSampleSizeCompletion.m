% simulation of sample complexity for matrix completion (revised)
%% Init
close all;clc; clearvars;
warning('on','all')

%% Set parameters
n1 = 100;       % number of rows in matrix
n2 = 50;        % number of columns in matrix
r = 5;          % rank 
d = 4;          % number of time stamps
sigma1 = 0.02;  % std variance of observation noise (Gaussian)
sigma2 = 0.1;   % std variance of perturbation noise (Gaussian)
p = 0.8;        % sampling ratio
repNum = 10;    % number of simulation runs
linkf = @gauss_link ; % link function, Gaussian for Gaussian observation model

reg.gamma = 0;  % l2 regularization for U
reg.mu = 0.1;   % l2 regularization for V
reg.iteMaxOut = 10; % maximum number of iterations for outer loop  
reg.tolOut = 1e-4; % optimality tolerance in objective
reg.ewflag = 0; % flag: 1 equal weights; 0 computed using dynamic ratio

% parameter space to sim in 
sigma2List = [logspace(-2.5,-1,7) logspace(-0.9,-0.2,6) logspace(-0.1,0.5,4)];
% sigma2List = [logspace(-2.5,-1,7) ];

pList = [ 0.05:0.02:0.2 0.22:0.03:1];

for sidx = 1:length(sigma2List)
    for pidx = 1:length(pList)
        sigma2 = sigma2List(sidx);
        p = pList(pidx);
        
        % Generate synthetic data 
        [U, Varray, Marray, Parray] = genSynNIPS(n1, n2, r, d, sigma1, sigma2, p, 1);
        % The true low-rank matrix
        UV= U * reshape(Varray(:,:,d),n2,r)';


        for i = 1:3
            if i == 1
                reg.kappa = 1e7; % baseline 1
            elseif i == 2
                reg.kappa = sigma2^2/sigma1^2; % LOWESS
            elseif i == 3
                reg.kappa = 1e-7; % baseline 2;
            end
            parfor repCnt = 1:repNum 
                % weighted

                [Uhat, Vhat, outputs] = weighted_alter_least_squares(Marray, ...
                    logical(Parray), r, linkf, [], [], reg);
                UVhat = Uhat*Vhat';
                relE(repCnt) = norm(UVhat - UV, 'fro')^2 ./ norm(UV, 'fro')^2;
            end  
            rel_w_2(i, sidx, pidx, :) = relE;
        end
    end
end
rel_error = sum(rel_w_2 , 4) / repNum;
%% Show and plot results
mark = {'*-', 'o-', '+-', 'v-','^-'};
lcolor = {'r', 'g', 'b', 'c', 'y'};
figure(1)
for i = 1:3
    for sidx = 1:length(sigma2List)
%         minErr = rel_error(i, sidx, end);
        % find the minimum complexity so that relative error < 0.04
        blist = find(rel_error(i, sidx,:) < 0.06);
        if isempty(blist)
            complexityList(sidx) = 1;
        else
            complexityList(sidx) = pList(blist(1));
        end
    end
    semilogx(sigma2List, complexityList,[mark{i} lcolor{i}]); hold on;
end
ltext{1} = 'Baseline one';
ltext{2} = 'LOWEMS';
ltext{3} = 'Baseline two';

l1 = xlabel('\sigma_2');
l2 = ylabel('Sample Complexity p');
l3 = legend(ltext);

set(gca,'FontSize',14);
set(l1,'FontSize',14);
set(l2,'FontSize',14);
set(l3,'FontSize',14);
