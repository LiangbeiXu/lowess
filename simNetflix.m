% Experiment on truncated netflix data
%% Init
clearvars; close all;
addpath('./utils/')
addpath('./data/')


%% generate training file and testing file from raw rating list
outputPath = './data/';
dataPath = './data/';
dataName = 'netflixtruncated';
dataFile = [dataPath dataName '.mat'];

sim.dataReady = 1;      % 1: data is already cut into time buckets
sim.p = 0.1;            % fraction of testing data
sim.inSample = 0;       % 1: in-sample test; 0: out of sample test
sim.repNum = 10;        % number of runs per experienment
algo = 'weighted_alter_least_squares'; 
sim.algo = algo;        % name of algorithm used 


reg.iteMaxOut = 15;     % maximum number of iterations for outer loop  
reg.tolOut = 1e-4;      % optimality tolerance in objective
reg.mu = 0;             % l2 regularization for U
reg.gamma = 0;          % l2 regularization for V
reg.kappa = 1e-1;       % dynamic ratio (see our paper)
reg.ewflag = 0;         % flag: 1 equal weights; 0 computed using dynamic ratio

para.mrank = 10;        % rank of the low-rank matrix
para.K = 5;             % K-fold cross-validation
para.cvNum = 1;         % default: K. to speed up validation set it < para.K  
para.inSample = sim.inSample;
para.dataPath = dataPath;
para.reg = reg;

% number of time bucket data is divided into
bucketNumList = [1 3 6 8];

% load data file (matrix form)
% columns 1~4: mvid, rating, date, userid 
load(dataFile);

datemin = min(ratings(:,3));
datemax = max(ratings(:,3));

ratingNum = size(ratings,1);
testNum = round(ratingNum*sim.p);

% sort records according to date
dateSorted = sort(ratings(:,3));

% 1-p/p  percentile date
dateMark = dateSorted(ratingNum - testNum);

% generate pivot date to cut 
if sim.inSample 
    dateMark = datemax;
end
sim.timeBucketList = create_bucket(bucketNumList, datemin,dateMark);


g=sprintf('%d ', bucketNumList);
fprintf('Number of bins : %s.\n',g);

% if data is not ready, generate data according to bucketNumList
if ~sim.dataReady
    [ trainFile, testFile ] = genMovieWeighted(outputPath, dataFile, ...
        sim.timeBucketList, sim.inSample, sim.p);
end

%% Run the Netflix experiment

figure(1);
mark = {'*-', 'o-', '+-', 'v-','^-'};
lcolor = {'r', 'g', 'b', 'c', 'y'};
for i = 1:length(bucketNumList)
    
    %% training (cross-validation)
    para.bucketNum = bucketNumList(i);
    fprintf('*******************Start evaluating d = %d********************\n', para.bucketNum);
    % specify the parameters that we want to run cross_sim with
    
    sim.simCnt = 0;
    sim.simCnt = sim.simCnt + 1;
    sim.paraList{sim.simCnt} = [1];
    sim.paraName{sim.simCnt} =  'reg.mu';


    sim.simCnt = sim.simCnt + 1;
    sim.paraList{sim.simCnt} = logspace(-1.5, 1, 8);
    sim.paraName{sim.simCnt} =  'reg.kappa';

    % pass para to sim for cross_sim
    sim.para = para;

    % run simulations in the specified parameter spaces
    funObj = @(para, algo ,paraName, paraChange) ...
    trainMovieWeightedWrapper( para, algo, paraName, paraChange );
    err = crossSim(sim, funObj);
   
    % dimension of simulation space
   	[comb, combIdx, combDim] = initComb(sim.paraList);
    
    % reshape the error matrix 
    temp = cell2mat(err);
    temp2 = reshape(temp,[],1);
    temp3 = [temp2.rmse];
    temp4 = reshape(temp3, combDim(1), combDim(2));
    
    % select minimum rmse error for each experienment
    [min_rmse, ~]= min(temp4, [], 1);
    
    % plot results 
    semilogx(sim.paraList{2}, min_rmse, [ mark{i} lcolor{i}]);
    ltext{i} = [ 'd = ' num2str(para.bucketNum) ]; 
    hold on;
    
    % clear temporary variables
    

    %% testing

    % set the parameters to the one achieving the best results
    temp = cell2mat(err);
    temp2 = reshape(temp,[],1);
    [min_rmse, idx_rmse]= min([temp2.rmse]);
    
    paraChange = comb(:,idx_rmse);
    for simIdx = 1:length(paraChange)
        eval(['para.' sim.paraName{simIdx}  '= paraChange(simIdx);']);
    end
    fprintf('Best parameter combinations (rmse=%f):\n',min_rmse);
    fprintf('mu=%f, gamma=%f, kappa=%f.\n',para.reg.mu, ...
        para.reg.gamma, para.reg.kappa);
 
    % set training data file path and testing data file path
    if para.inSample
        trainData = [dataPath 'ratMatrixFutureIn_' num2str(bucketNumList(i)) '.mat'];
    else
        trainData = [dataPath 'ratMatrixFutureOut_' num2str(bucketNumList(i)) '.mat'];
    end
    if para.inSample
        testData = [dataPath 'testMatrixIn_' num2str(bucketNumList(i))  '.mat'];
    else
        testData = [dataPath 'testMatrixOut' '.mat'];
    end
    
    parfor j = 1:sim.repNum
        tic
        predErr{j,i} = predMovieWeighted(trainData, testData, para, algo);
        toc
        fprintf('testing error (rmse) %f.\n', predErr{j,i}.rmse);
    end
    % update the parameter used to predict on test data
    sim.save.para{i} = para;
    sim.save.trErr{i} = err;
    sim.save.teErr = predErr;
    
    clear temp temp2 temp3 temp4;
end

%% show, plot and save results 
saveDir = './save/';

% show and save intermediate results
l1 = xlabel('\kappa');
l2 = ylabel('RMSE');
set(gca,'FontSize',14);
set(l1,'FontSize',14);
set(l2,'FontSize',14);
l3 = legend(ltext);
set(l3,'FontSize',14);
savefig(gcf,[saveDir dataName '_' sim.algo '_' num2str(para.mrank) '_kappa' '.fig']);

% show and save results
temp = cell2mat( sim.save.teErr);
figure(2)
boxplot( reshape([temp.rmse],sim.repNum,[]), bucketNumList(1:end));
l1 = xlabel('# of bins');l2 = ylabel('RMSE');
set(gca,'FontSize',14);
set(l1,'FontSize',14);
set(l2,'FontSize',14);

% save figs
savefig(gcf,[saveDir dataName '_' sim.algo '_' num2str(para.mrank)  '.fig']);

% save experiments
save([saveDir dataName '_' sim.algo '_' num2str(para.mrank) '.mat'], 'sim');