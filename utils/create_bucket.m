function timeBucketList = create_bucket (bucketNumList, tmin, tmax)
% create the pivot timestamps based on the number of buckets

timeBucketList = cell(length(bucketNumList),1);
for i = 1:length(bucketNumList)
    b = bucketNumList(i);
    step = round((tmax - tmin)/b) + 1;
    for j = 1:b+1
        if j == b+1
            timeBucketList{i}(j) = tmax+1;
        else
            timeBucketList{i}(j) = tmin + (j-1) * step;
        end
    end
end