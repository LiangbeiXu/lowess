function [comb, combIdx, combDim] = initComb(paraList)
% generate parameter combinations
comb = [];
combIdx = [];
combDim = [];
simCnt = length(paraList);
for simIdx = 1:simCnt
    if isempty(comb)
        comb = paraList{simIdx};
        combDim = length(paraList{simIdx});
        combIdx = 1:combDim;
    else 
        comb = combvec(comb, paraList{simIdx});
        combIdx = combvec(combIdx,1:length(paraList{simIdx}));
        combDim = [combDim length(paraList{simIdx})];
    end
end