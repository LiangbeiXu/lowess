function [L, rMean, uMean, mMean] = baselineModel(ratingMatrix, alpha)
%% Form a baseline matrix: (1-alpha) * user_aveg + alpha * mv_aveg.

[mvNum, userNum, bucketNum] = size(ratingMatrix);


rated = ( ratingMatrix~=0); % rated indicate
ratingNum = sum(sum(sum(rated,1),2),3); % # of ratings

rMean = sum(sum(sum(ratingMatrix,1),2),3)/ratingNum;
uMean = zeros(1,userNum);
for i = 1:userNum
    if sum((ratingMatrix(:,i)~=0)) == 0 
        uMean(i) = rMean;
    else
        uMean(i) = sum(ratingMatrix(:,i)) / sum((ratingMatrix(:,i)~=0));
    end
end
mMean = zeros(mvNum,1);
for i = 1:mvNum
    if sum((ratingMatrix(i,:)~=0)) == 0  % movie is not rated by user
        mMean(i) = rMean;
    else
        mMean(i) = sum(ratingMatrix(i,:)) / sum((ratingMatrix(i,:)~=0));
    end

end

L1 = repmat(mMean, 1,userNum,bucketNum);
L2 = repmat(uMean, mvNum, 1, bucketNum);

% L = repmat(rMean,mvNum, userNum, bucketNum);
L = (1-alpha)*L1 + alpha*L2; % baseline ratings
