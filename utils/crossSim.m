function errList = crossSim(sim, funObj)
%% Perform multiple simulations for all parameter combinations
% sim is tructure. 
    % sim.simCnt    : number of parameters to cross simulation
    % sim.repNum    : run simulations multiple times to ensure statistical
    %               significant
    % sim.para      : parameters passed onto funobj
    % sim.paraList  : list of paramters values in cross simulation
    % sim.paraName  : name of parameters in cross simulation.
    % sim.paraIte   : parameter for algorithms, usually fixed
% funobj            : functions that is simulated
% predErrList       : simulation metric returned

%% cross parameter combination
    [comb, combIdx, combDim] = initComb(sim.paraList);
    paraName = sim.paraName;
    runNum = size(comb,2);
    if length(combDim) == 1
        errList = cell(combDim,1);
    else
        errList = cell(combDim);
    end
      
    for  runIdx = 1:runNum
        tic
        errList{runIdx} =  funObj(sim.para, ...
            sim.algo, paraName,comb(:,runIdx));
        toc
        for h = 1:length(combDim)
            fprintf([paraName{h} '=' num2str(combIdx(h,runIdx)) ';']);
        end
        fprintf('\n'); 

    end
end