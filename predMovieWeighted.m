function [err, outputs] = predMovieWeighted(trdata, tedata, para, algo)
%% Train using training data and predict using testing data

% parameters used to pred 
reg = para.reg;
mrank = para.mrank;
inSample = para.inSample;


%% begin training

load(trdata) % load training data -- ratingMatrixTemp
load(tedata) % load testing data -- testMatrix

Y = ratingMatrixTemp; clear ratingMatrixTemp
[nItems, nUsers, d] = size(Y);
% sampling matrix for training data
trainP = (Y~=0);

T = testMatrix; clear testMatrix
% sampling matrix for testing data
testP = (T~=0);

% baseline: 0.5 * user_aveg + 0.5 * mv_aveg
L = baselineModel (Y.*trainP, 0.5);

%  de-biased rating matrix 
B = Y - L;

% specify link function
linkf = @ gauss_link ;

% apply the same regularization on U and V
reg.gamma = reg.mu; 

tic
switch(algo)

    case 'alter_least_squares'
        [U, V, outputs] = weighted_alter_least_squares(B, logical(trainP), mrank, ...
            linkf, [], [], reg);
    otherwise % other algorithms
        [U, V, outputs] = weighted_alter_least_squares(B, logical(trainP), mrank, ...
            linkf, [], [], reg);
end
toc

% generate prediction matrix
if inSample
    UV = U*V';
    Yhat = reshape(L(:,:,1),nItems, nUsers) + UV;
else
    if d == 1
        UV=  U*V';
        Yhat = L + UV;
    else
        UV=  U*V';
        Yhat = L(:,:,1) + UV;
    end
end

% truncate ratings to [1, 5]
Yhat(Yhat>5) = 5;
Yhat(Yhat<=1) = 1;

% calculate rmse
err.rmse = ( sum( sum(sum(  abs(Yhat - T).^2.*testP ,3 ),2),1) / ...
    sum(sum(sum(  testP ,3 ),2),1) ).^0.5; 
