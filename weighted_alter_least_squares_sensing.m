function [U, V, outputs] = weighted_alter_least_squares_sensing(Y, P, k, U0, V0, reg)
%% Alternating least squres with weights
%
% Y         : m0-by-d observation matrix 
% P         : m0-by-d sampling matrix
% k         : rank 
% U0        : init U
% V0        : init V
% reg       : some parameters used in alternating minimization, with fields
    % mu        : l2 regularization for U
    % gamma     : l2 regularization for V
    % kappa     : dynamic ratio = sigma_2^2 / sigma_1^2
    % iteMaxOut : maximum number of iterations for outer loop
    % tolOut    : optimality tolerance in objective
    % n1        : number of rows in matrix
    % n2        : number of columns in matrix

% U         : estimated U
% V         : estimated V
% outputs   : intermediate outputs structure
   
n1 = reg.n1;
n2 = reg.n2;
[d, m0] = size(Y);

% init U and V
if isempty(U0) U = randn(n1, k); else U = U0; end
if isempty(V0) V = randn(n2, k); else V = V0; end

% calculate weights
if ~reg.ewflag  
    a = zeros(d,1);
    for t = 1:d
        a(d-t+1) = 1+ (t-1)*reg.kappa; 
    end
    b = sum(1./a);
    for t = 1:d
        w(t) = 1/b * 1/(a(t));
    end
    else
    w = 1/d *ones(d,1);
end


% starting outer loop
iteCntOut = 0;

% initial objective function
obj = obj_function(U, V, Y, P, reg, w);
outputs.obj(iteCntOut + 1) = obj;

% pre-allocate intermediate varibale for least squares 
Av = zeros(m0*d,n1*k);
Au = zeros(m0*d,n2*k);
yv = zeros(m0*d,1);
yu = zeros(m0*d,1);
while iteCntOut < reg.iteMaxOut
    
    % update U with V fixed using least squares
    for t = 1:d
        for i = 1:m0 
            temp = reshape(P(:,:,t,i),n1,n2);
            Av ((t-1)*m0+i,:) = w(t).^0.5 * reshape( temp*V, 1, []) ;
            yv ((t-1)*m0+i) = w(t).^0.5 * Y(t,i);
        end
    end
    U = reshape( inv(Av'*Av + reg.mu* eye(n1*k,n1*k)) * Av' * yv, n1,k);
    
    % update V with U fixed using least squares
    for t = 1:d
        for i = 1:m0 
            temp = reshape(P(:,:,t,i),n1,n2);
            Au ((t-1)*m0+i,:) = w(t).^0.5 * reshape( temp'*U, 1, []) ;
            yu ((t-1)*m0+i) = w(t).^0.5 * Y(t,i);
        end
    end
    V = reshape( inv(Au'*Au + reg.gamma* eye(n2*k,n2*k)) * Au' * yu, n2,k);

    last_obj = obj;
    obj = obj_function(U, V, Y, P, reg, w);
    rel_obj = abs(obj - last_obj) / abs(last_obj);
    if rel_obj < reg.tolOut
        % exit when objective don't change much 
        break;
    end
    iteCntOut = iteCntOut + 1;
    outputs.obj(iteCntOut + 1) = obj;    
end

end


% objective function
function obj = obj_function(U, V, Y, P, reg, w)
    
    n1 = reg.n1;
    n2 = reg.n2;
    [d, m0] = size(Y);
    nllh = 0;
    for i = 1:d
        tmp =  U* V';
        tmp2 = repmat(tmp,1,1,m0);
        Ptmp = reshape(P(:,:,i,:),n1,n2,m0);
        ytmp = reshape( sum(sum(tmp2.*Ptmp,1),2),1,[]);
        % negative log likelihood 
        nllh =   nllh  + 0.5 * w(i) * sum((Y(i,:) - ytmp).^2);
    end
    obj = nllh + 0.5 * reg.mu * norm(U, 'fro')^2 +  0.5 *  reg.gamma * norm(V, 'fro')^2 ;
end