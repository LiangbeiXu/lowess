function [U, Varray, Marray, Parray] = genSynNIPS(n1, n2, k, d, sigma1, ...
    sigma2, p, Replacement)
%% Generate synthetic data

%%%% input 
% n1            : column dimension 
% n2            : row dimension 
% k             : rank 
% d             : time stamps
% sigma1        : observation noise
% sigma2        : innovation noise
% p             : percentage of observed entries
% Replacement   : '1'('0'): uniformly sampling with (without) replacement


%%% output
% Varray        : n2-by-k-by-d matrix, time varying V
% U             : n1-by-k matrix, time independent U
% Marray        : n1-by-n2-by-d matrix, generated low-rank matrix with noise
% Parray        : n1-by-n2-by-d matrix, observation indication matrix


%%
% Generate U  in [-0.5 0.5] and orthnomalize it
U = orth(rand(n1,k) - 0.5);

% Generate initial V0  in [-0.5 0.5] and orthnomalize it
V0 = rand(n2,k) - 0.5;

% Allocate  outputs
Marray = zeros(n1,n2,d);
Parray = zeros(n1,n2,d);
Varray = zeros(n2,k,d);

if nargin < 10, Replacement = 1;end

% we will set V_d to constant variance.
for i = d:-1:1
    if i == d
        V = V0;
    else    
        % Gaussian random walk
        V = V + randn(n2,k)*sigma2 ; 
    end
    
    if  Replacement
        % generate sampling matrix for with replacement sampling model
        S = randsample(n1*n2,round(p*n1*n2),'true'); 
        [C,uS]=hist(S,unique(S)); % C is the counts vector
        P = zeros(n1,n2);
        P(uS) = 1;
        W = zeros(n1,n2);
        W(uS) = C;
    else
        % generate sampling matrix for without replacement sampling model
        omega = rand(n1,n2);
        % observation matrix P
        P = zeros(n1,n2);
        P(omega>1-p) = 1;
        P(P<1) = 0; 
        W = ones(n1, n2);
    end
    N = U*V'; % matrix r(N) = k;
    Y = randn(n1,n2)*sigma1.*W.^0.5; % add gaussian noise to observation
    
    Marray(:,:,i) = N+Y;
    Parray(:,:,i) = P;
    Varray(:,:,i) = V;
end 