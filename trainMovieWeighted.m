function [err] = trainMovieWeighted( para, algo )
% Train and cross validate on specified parameters

% parameters used to train 
inSample = para.inSample;
bucketNum = para.bucketNum;
reg = para.reg;
mrank = para.mrank;
K = para.K;
dataPath = para.dataPath;
cvNum = para.cvNum;


% load data
if inSample
    load([dataPath 'ratMatrixFutureIn_' num2str(bucketNum) '.mat']);
else
    load([dataPath 'ratMatrixFutureOut_' num2str(bucketNum) '.mat']);
end
Y = ratingMatrixTemp; clear ratingMatrixTemp

[nItems, nUsers, d] = size(Y);

% sampling matrix
P = zeros(nItems, nUsers, d); 
P = (Y~=0);

% baseline: 0.5 * user_aveg + 0.5 * mv_aveg
L = baselineModel (Y.*P, 0.5);

%  de-biased rating matrix 
B = Y - L;

rmseList = zeros(cvNum,1);
linkf = @ gauss_link ;
randm = randi(K, size(B) );
randt = zeros(size(B));
for i = 1:cvNum
    % split data
    randt(randm==i) = 1;
    randt(randm~=i) = 0;
    
    % only keep data in the last bin as validation data
    randt(:,:,1:end-1) = 0;
    trainP = P.*(1-randt);
    valP = P.*randt;
    
    % apply the same regularization on U and V
    reg.gamma = reg.mu; 

    tic
    switch(algo)

        case 'weighted_alter_least_squares'
            [U, V, outputs] = weighted_alter_least_squares(B, logical(trainP), mrank, ...
                linkf, [], [], reg);
        otherwise % Implement other algorithms
            [U, V, outputs] = weighted_alter_least_squares(B, logical(trainP), mrank, ...
                linkf, [], [], reg);
    end
    toc
    
    UV = zeros(size(U,1),size(V,1),d);
    % using recovery U,V in the last bin to recover
    for t = 1:d
        UV(:,:,t) = U*V';
    end
    
    % restore ratings
    Yhat = L + UV;
    
    % truncate ratings to [1, 5]
    Yhat(Yhat>5) = 5;
    Yhat(Yhat<=1) = 1;

    % calculate rmse
    rmseList(i) = ( sum( sum(sum(  abs(Yhat - Y).^2.*valP ,3 ),2),1) / ...
        sum(sum(sum(  valP ,3 ),2),1) ).^0.5; 
end

err.rmse = mean(rmseList);

% plot(2:length(outputs.obj), outputs.obj(2:end));
% xlabel('Iterations');ylabel('cost');title('Outer loop');
