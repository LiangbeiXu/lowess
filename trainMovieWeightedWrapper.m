function [err] =trainMovieWeightedWrapper( para, algo, paraName, paraChange)
%% A wrapper for function trainMovieWeighted

% set parameters
for simIdx = 1:length(paraChange)
    eval(['para.' paraName{simIdx}  '= paraChange(simIdx);']);
end

% do experiment
[err] = trainMovieWeighted(para, algo);

