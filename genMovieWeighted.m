function [ trainFile, testFile ] = genMovieWeighted(outputPath, dataFile, timeBucketList, inSample, p)
%% generate appropriate input for LOWESS algorithm
% Convert transaction-type input to matrix-type input

%%%% input 
% outputPath        : output dir 
% dataFile          : data dir+filename
% timeBucketList    : a list of pivot time stamps 
% inSample          : 1: in-sample test; 0: out of sample test
% p                 : fraction of testing data

%%% output
% trainFile         : training data file name
% testFile          : testing data file name


%% Temporal distribution of ratings
load(dataFile)
ratings = double(ratings);
hist(ratings(:,3),100);
datemin = min(ratings(:,3));
datemax = max(ratings(:,3));
dateSorted = sort(ratings(:,3));

%% keep  p out of 1 for testing
ratingNum = size(ratings,1);
testNum = round(ratingNum * p);
dateMark = dateSorted(ratingNum - testNum);


if inSample 
    dateMark = datemax;
end

userid  = unique(ratings(:,1));
userNum = length(userid);
usermap = containers.Map(userid, 1:userNum); 

mvid = unique(ratings(:,4));
mvNum = length(mvid) ;
mvmap = containers.Map(mvid, 1:mvNum);

% initialize rating matrix and test matrix
ratingMatrix = cell(length(timeBucketList),1);
for j = 1:length(timeBucketList)
    bucketNum = length(timeBucketList{j})-1;
    ratingMatrix{j} = zeros(mvNum, userNum, bucketNum);
end
if inSample
    testMatrixCell = cell(length(timeBucketList),1);
    for j = 1:length(timeBucketList)
        bucketNum = length(timeBucketList{j})-1;
        testMatrixCell{j} = zeros(mvNum, userNum);
    end
else
    testMatrix = zeros(mvNum, userNum);
end
   

randTemp = rand(ratingNum,1);
randTemp (randTemp > (1-p)) = 1;
randTemp (randTemp < 1) = 0;

for i = 1:size(ratings,1)
    uidx = usermap(ratings(i,1));
    midx = mvmap(ratings(i,4));
    if inSample
        if randTemp(i) 
            for j = 1:length(timeBucketList)
                timeBucket = timeBucketList{j};
                tidx = find (ratings(i,3) - timeBucket < 0, 1, 'first') -1;
                if tidx == (length(timeBucket) - 1)  % fall in last bin
                    testMatrixCell{j}(midx, uidx) = ratings(i,2);
                else
                    ratingMatrix{j}(midx, uidx, tidx) = ratings(i,2);
                end
            end
        else
            for j = 1:length(timeBucketList)
                timeBucket = timeBucketList{j};
                tidx = find (ratings(i,3) - timeBucket < 0, 1, 'first') -1;
                ratingMatrix{j}(midx, uidx, tidx) = ratings(i,2);
            end
        end
    else
        if ratings(i,3) > dateMark
            testMatrix(midx, uidx) = ratings(i,2);
        else
            for j = 1:length(timeBucketList)
                timeBucket = timeBucketList{j};
                tidx = find (ratings(i,3) - timeBucket < 0, 1, 'first') -1;
                ratingMatrix{j}(midx, uidx, tidx) = ratings(i,2);
            end
        end
    end
end

% save training file and testing file
for j = 1:length(timeBucketList)
    ratingMatrixTemp = ratingMatrix{j};
    bucketNum = length(timeBucketList{j})-1;
    if inSample
        trainFile{j} = ['ratMatrixFutureIn_' num2str(bucketNum) '.mat'];
    else
        trainFile{j} = ['ratMatrixFutureOut_' num2str(bucketNum) '.mat'];
    end
    save([outputPath trainFile{j}],'ratingMatrixTemp');
end
if inSample
    for j = 1:length(timeBucketList)
        testMatrix = testMatrixCell{j};
        bucketNum = length(timeBucketList{j})-1;
        testFile{j} = ['testMatrixIn_' num2str(bucketNum) '.mat'];
        save([outputPath testFile{j}],'testMatrix');
    end
else
    for j = 1:length(timeBucketList)
        testFile{1} = ['testMatrixOut' '.mat'];
        save([outputPath testFile{1}],'testMatrix');
    end
end




