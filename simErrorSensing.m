% simulation of recovery error for Gaussian measurement ensemble (revised)
%% Init
clear; close all;clc

%% Set parameters
n1 = 100;               % number of rows in matrix
n2 = 50;                % number of columns in matrix
r = 5;                  % rank 
d = 4;                  % number of time stamps
sigma1 = 0.05;          % std variance of observation noise (Gaussian)
sigma2 = 0.1;           % std variance of perturbation noise (Gaussian)
m0 = 4000;              % number of observations per time stamp
repNum = 10;            % number of simulation runs

reg.gamma = 0;          % l2 regularization for U
reg.mu = 0;             % l2 regularization for V
reg.iteMaxOut = 10;     % maximum number of iterations for outer loop  
reg.tolOut = 1e-4;      % optimality tolerance in objective
reg.ewflag = 0;         % flag: 1 equal weights; 0 computed using dynamic ratio

% passing dimension parameter through reg structure
reg.n1 = n1;
reg.n2 = n2;


% parameter space to sim in
sigma2List = logspace(-2,0,15);

% kappaList represents: baseline 1, LOWESS, baseline 2
kappaList  = [1e7 1 1e-7];
tic
%% run simulations
for sidx = 1:length(sigma2List)
    sigma2 = sigma2List(sidx);
    
    % Generate synthetic data
    U = orth(randn(n1,r));
    V0 = (randn(n2,r));
    Y = nan(n1, n2, d);
    for i = d:-1:1
        if i == d
            V = V0;
        else  
            V = V +randn(n2,r)*sigma2;
        end
        Varray(:,:,i) = V;
        N = U*V'; % matrix r(N) = k;
        Y(:,:,i) = N; 
    end
    
    % Generating observation using Gaussian ensemble model
    M2 = randn(n1,n2,d,m0) / m0^0.5;
    Y2 = zeros(d, m0);
    for i = 1:m0
        M = M2(:,:,:,i);
        Y2(:,i) = reshape( sum(sum(Y.*M, 1),2),[],1) + randn(d,1)*sigma1;
    end
    UV= U * reshape(Varray(:,:,d),n2,r)';
    kappaList(2) = sigma2^2/sigma1^2;
    
     % recover
    for kidx = 1:length(kappaList)
        reg.kappa = kappaList(kidx);
        
        % use parfor to speedup
        parfor repCnt = 1:repNum  
%         for repCnt = 1:repNum            
            [Uhat, Vhat, outputs] = weighted_alter_least_squares_sensing(...
                Y2, M2, r, [], [], reg);
            UVhat = Uhat*Vhat';
            relE(repCnt) = norm(UVhat - UV, 'fro')^2 ./ norm(UV, 'fro')^2;
        end
        rel_error(kidx , sidx) = mean(relE);
    end
end
toc
%% Show and plot results
figuremark = {'*-', 'o-', '+-', 'v-','^-'};
lcolor = {'r', 'g', 'b', 'c'};

% Baseline 1
semilogx(sigma2List, rel_error(1,:), [mark{1} lcolor{1}]);  hold on

% Baseline 2
semilogx(sigma2List(1:10), rel_error(end,1:10), [mark{3} lcolor{3}]); hold on

% LOWEMS
semilogx(sigma2List, rel_error(2,:), [mark{2} lcolor{2}]);  


ltext{1} = 'Baseline one';
ltext{2} = 'Baseline two';
ltext{3} = 'LOWEMS';
l1 = xlabel('\sigma_2');
l2 = ylabel('Recovery Error');
l3 = legend(ltext);

set(gca,'FontSize',14);
set(l1,'FontSize',14);
set(l2,'FontSize',14);
set(l3,'FontSize',14);

