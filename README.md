
* Matlab files for simulations and experiments for paper "Dynamic matrix recovery from incomplete 
  observations under an exact low-rank constraint"
  
* Version 1.0

* simErrorCompletion.m
	Recovery error simulations for matrix completion
* simErrorSensing.m
	Recovery error simulations for matrix sensing
* simSampleSizeCompletion.m
	Sample complexity simulations for matrix completion
* simNetflix.m
	Truncated Netflix dataset experiement

* Contact: Liangbei.xu@gmail.com OR mdav@gatech.edu